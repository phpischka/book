@extends('layouts.master')
@section('title', 'Authors')
@section('content')
<h1>Authors</h1>
<a href="{{url('authors/create')}}" class="btn btn-success btn-sm mt-3" tabindex="-1" >Add</a>
<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Books</th>
            <th scope="col">Actions</th>
            
        </tr>
    </thead>
    <tbody>
        @foreach($authors as $author)
            <tr>
                <th scope="row">{{$author->id}}</th>
                <td>{{$author->name}}</td>
                <td>
                    @foreach($author->books as $book)
                        <p>{{$book->title}}</p>
                    @endforeach
                </td>
                <td>
                   <a href="{{'authors/' . $author->id . '/edit'}}" class="btn btn-primary btn-sm" tabindex="-1" >Edit</a>
                   
                   <form method="post" action="{{url('authors/' . $author->id)}}">
                       @csrf
                       {{method_field('DELETE')}}
                       <button type="submit" class="btn btn-danger btn-sm mt-1">Delete</button>
                   </form> 
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection
