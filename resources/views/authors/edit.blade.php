@extends('layouts.master')
@section('title', 'Author Edit')
@section('content')
<h1>Author Edit</h1>

<form method="post" class="mt-3" action="{{url('authors/' . $author->id)}}">
    @csrf
    {{method_field('PUT')}}
    <div class="mb-3">
        <label for="exampleInputName" class="form-label">Name</label>
        <input type="text" name="name" value="{{$author->name}}" class="form-control @error('name') is-invalid @enderror" id="exampleInputName" >
        @error('name')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
