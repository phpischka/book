<!doctype html>
<html lang="ru">
    <head>
        <!-- Обязательные метатеги -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">

        <title>@yield('title')</title>
    </head>
    <body>
        <div class="container">
            @include('partials.header')
            @yield('content')
        </div>
        @stack('scripts')
    </body>
</html>