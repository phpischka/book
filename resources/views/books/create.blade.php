@extends('layouts.master')
@section('title', 'Book create')
@section('content')
<h1>Book Create</h1>

<form method="post" class="mt-3" action="{{url('books')}}">
    @csrf
    <div class="mb-3">
        <label class="form-label">Author</label>
        <select name="author_id" class="form-select @error('author_id') is-invalid @enderror"  aria-label="Default select example">
          <option></option>
          @foreach($authors as $author)
            @if($author->id == old('author_id'))
                <option selected value="{{$author->id}}">{{$author->name}}</option>
            @else
                <option value="{{$author->id}}">{{$author->name}}</option>
            @endif
          @endforeach
        </select>
        @error('author_id')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
        <label for="exampleInputTitle" class="form-label mt-3">Title</label>
        <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" id="exampleInputTitle" >
        @error('title')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
