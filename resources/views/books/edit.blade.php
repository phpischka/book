@extends('layouts.master')
@section('title', 'Book Edit')
@section('content')
<h1>Book Edit</h1>

<form method="post" class="mt-3" action="{{url('books/' . $book->id)}}">
    @csrf
    {{method_field('PUT')}}
    <div class="mb-3">
        <label class="form-label">Book</label>
        <select name="author_id" class="form-select @error('author_id') is-invalid @enderror"  aria-label="Default select example">
          <option></option>
          @foreach($authors as $author)
            @if($author->id == $book->author->id)
                <option selected value="{{$author->id}}">{{$author->name}}</option>
            @else
                <option value="{{$author->id}}">{{$author->name}}</option>
            @endif
          @endforeach
        </select>
        @error('author_id')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
        <label for="exampleInputTitle" class="form-label mt-3">Title</label>
        <input type="text" name="title" value="{{$book->title}}" class="form-control @error('title') is-invalid @enderror" id="exampleInputTitle" >
        @error('title')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
