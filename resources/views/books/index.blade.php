@extends('layouts.master')
@section('title', 'Books')
@section('content')
<h1>Books</h1>
<a href="{{url('books/create')}}" class="btn btn-success btn-sm mt-3" tabindex="-1" >Add</a>
<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Author</th>
            <th scope="col">Actions</th>
            
        </tr>
    </thead>
    <tbody>
        @foreach($books as $book)
            <tr>
                <th scope="row">{{$book->id}}</th>
                <td>{{$book->title}}</td>
                <td>
                    {{$book->author->name}}
                </td>
                <td>
                   <a href="{{'books/' . $book->id . '/edit'}}" class="btn btn-primary btn-sm" tabindex="-1" >Edit</a>
                   
                   <form method="post" action="{{url('books/' . $book->id)}}">
                       @csrf
                       {{method_field('DELETE')}}
                       <button type="submit" class="btn btn-danger btn-sm mt-1">Delete</button>
                   </form> 
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection
